<?php
/**
 * Copyright ©   All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Tezus\Parcelamento\Model\Config\Source;

class OpcoesParcelas implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
        ['value' => '1', 'label' => '1'],
        ['value' => '2', 'label' => '2'],
        ['value' => '3', 'label' => '3'],
        ['value' => '4', 'label' => '4'],
        ['value' => '5', 'label' => '5'],
        ['value' => '6', 'label' => '6'],
        ['value' => '7', 'label' => '7'],
        ['value' => '8', 'label' => '8'], 
        ['value' => '9', 'label' => '9'], 
        ['value' => '10', 'label' =>'10'], 
        ['value' => '11', 'label' => '11'], 
        ['value' => '12', 'label' => '12']
    ];
    }

    public function toArray()
    {
        return [
        '1'=> '1',
        '2'=> '2',
        '3'=> '3',
        '4'=> '4',
        '5'=> '5',
        '6'=> '6',
        '7'=> '7',
        '8'=> '8', 
        '9'=> '9', 
        '10'=> '10', 
        '11'=> '11', 
        '12'=> '12' 
    ];
    }
}
