<?php
/**
 * Copyright ©   All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Tezus\Parcelamento\Model\Config\Source;

class TiposJuros implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
  
        ['value' => 's', 'label' => 'Simples'], 
        ['value' => 'c', 'label' => 'Composto']
    ];
    }

    public function toArray()
    {
        return [

        's'=> 'Simples', 
        'c'=> 'Composto' 
    ];
    }
}

