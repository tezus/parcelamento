<?php
/**
 * Copyright ©   All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Tezus\Parcelamento\Model\Config\Source;

class OpcoesJuros implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
        ['value' => 'n', 'label' => 'Sem Juros'],
        ['value' => '1', 'label' => '1x'],
        ['value' => '2', 'label' => '2x'],
        ['value' => '3', 'label' => '3x'],
        ['value' => '4', 'label' => '4x'],
        ['value' => '5', 'label' => '5x'],
        ['value' => '6', 'label' => '6x'],
        ['value' => '7', 'label' => '7x'],
        ['value' => '8', 'label' => '8x'], 
        ['value' => '9', 'label' => '9x'], 
        ['value' => '10', 'label' =>'10x'], 
        ['value' => '11', 'label' => '11x'], 
        ['value' => '12', 'label' => '12x']
    ];
    }

    public function toArray()
    {
        return [
        'n'=> 'Sem Juros',
        '1'=> '1x',
        '2'=> '2x',
        '3'=> '3x',
        '4'=> '4x',
        '5'=> '5x',
        '6'=> '6x',
        '7'=> '7x',
        '8'=> '8x', 
        '9'=> '9x', 
        '10'=> '10x', 
        '11'=> '11x', 
        '12'=> '12x' 
    ];
    }
}
