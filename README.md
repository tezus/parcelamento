# Mage2 Module Tezus Correios

    ``tezus/module-correios``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Módulo para o cálculo de frete e prazo dos correios para lojas em Magento 2.

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Tezus`
 - Enable the module by running `php bin/magento module:enable Tezus_Correios`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require tezus/module-correios`
 - enable the module by running `php bin/magento module:enable Tezus_Correios`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Correios - carriers/correios/*

 - Habilitado (configuracoes/configuracoes/habilitado)

 - Opções (correios/configuracoes/opcoes)

 - Método Gratuito (correios/configuracoes/metodo_gratuito)

 - Número do Contrato (correios/configuracoes/numero_contrato)

 - Senha do Contrato (correios/configuracoes/senha_contrato)

 - Unidade de Peso (correios/configuracoes/unidade_de_peso)

 - Mãos Próprias (correios/configuracoes/maos_proprias)

 - Aviso de Recebimento (correios/configuracoes/aviso_de_recebimento)

 - Usar Valores Declarados (correios/configuracoes/usar_valores_declarados)

 - Validar Dimensões (correios/configuracoes/validar_dimensoes)

 - Largura Padrão (correios/configuracoes/largura_padrao)

 - Altura Padrão (correios/configuracoes/altura_padrao)

 - Profundidade Padrão (correios/configuracoes/profundidade_padrao)

 - Taxa de Manuseio (correios/configuracoes/taxa_de_manuseio)

 - Exibir Prazo de Entrega (correios/configuracoes/exibir_prazo_de_entrega)

 - Incrementar Dias ao Prazo (correios/configuracoes/incrementar_dias_ao_prazo)

 - URL do WebService (correios/configuracoes/url_do_webservice)

 - Peso Máximo (correios/configuracoes/peso_maximo)


## Specifications

 - Shipping Method
	- correios

 - Block
	- Calculo > calculo.phtml


## Attributes

 - EAV (custom) - Altura Correios (altura_correios)

 - EAV (custom) - Largura Correios (largura_correios)

 - EAV (custom) - Dias adicionais para entrega (dias_adicionais_para_entrega)

