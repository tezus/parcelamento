<?php
namespace Tezus\Parcelamento\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;


class Data extends AbstractHelper
{
     /**
   * @var \Magento\Framework\App\Config\ScopeConfigInterface
   */
  protected $scopeConfig;

  /**
  *config path
  */
  

  public function __construct(
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Catalog\Model\ProductRepository $productRepository

      )
  {
   
 
    $this->productRepository = $productRepository;
    $this->scopeConfig = $scopeConfig;

  }

  /**
  * returning config value
  **/

   public function getConfig($path) {
    $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

    return $this->scopeConfig->getValue($path, $storeScope);


    }
    

   
}?>