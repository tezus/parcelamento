<?php
namespace Tezus\Parcelamento\Block;
class Parcelamento extends \Magento\Framework\View\Element\Template
{
	
	protected $helperData;
	protected $maxParcelas;
	protected $valorMinimo;
	protected $taxa;
	protected $jurosad;
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Tezus\Parcelamento\Helper\Data $helperData
	)
	{
		$this->helperData = $helperData;
	
		parent::__construct($context);
		$this->maxParcelas = $this->helperData->getConfig('parcelamento/configuracoes/numero_de_parcelas');
		$this->valorMinimo = $this->helperData->getConfig('parcelamento/configuracoes/valor_minimo');
		$this->taxa = $this->helperData->getConfig('parcelamento/configuracoes/taxa');
		$this->jurosad = $this->helperData->getConfig('parcelamento/configuracoes/juros_a_partir_de');
		$this->tipoJ = $this->helperData->getConfig('parcelamento/configuracoes/tipo_de_juros');
		
	}
	public function getNumParcelas($total)

    {
		$n = $this->maxParcelas;
		$valor_minimo = $this->valorMinimo;
	
    
		//verifica o valor mínimo permitido para cada parcela
		if (!empty($valor_minimo) ) {
		$parcPossiveis = floor($total / $valor_minimo);
		
		if ($parcPossiveis < $n) {
		$n = $parcPossiveis;
		}
	}

	return $n;
}

   public function getValorParcela($total, $parcela) {
        if(!is_numeric($total) || $total <= 0){
            return(false);
            }
            if((int)$parcela != $parcela){
            return(false);
			}
			$taxa = $this->taxa;
           
          
          
            
            $denominador = 0;
            if($parcela > 1){
				
				
				
                for($i=1; $i<=$parcela; $i++){
                    
                        if($i >= $this->jurosad){
						
 							 $taxinha = ( $taxa * $total ) / 100;
							 $denominador = ($total+$taxinha)/$i;
							
								

							
						}else{
								 $taxinha = ( 0.00 * $total ) / 100;
                             $denominador = ($total+$taxinha)/$i;
						}
					}
                  
                    
                //     elseif($i == 4){
                //         $taxinha = ( 3.98 *  $total) / 100;
                //         $denominador = ($total+$taxinha)/4;
                // }
                //     elseif($i == 5){
                //         $taxinha = ( 5.97 * $total ) / 100;
                //         $denominador = ($total+$taxinha)/5;
                       
                // }
                //     elseif($i == 6){
                //         $taxinha = ( 7.96 * $total ) / 100;
                //         $denominador = ($total+$taxinha)/6;
                       
                // }
                  
                    }
						
      
            // for($i=5; $i<=$parcela; $i++){
            // $denominador += 1/pow(1+1.99,$i);
            // }
            
            else{
            $denominador = 1;
			}
			return($denominador);
            }
            //return(round($total/$denominador));
            
    


	public function parsela($valo)
	{
		$parsela ='<small>';  
		// máximo de parcelas
	  
	   $capital = $valo;
	  
	   $numParcelas = $this->getNumParcelas($capital);
	//    var_dump($numParcelas);
	   for($i=1; $i < $numParcelas; $i++){
			$valorParcela = $this->getValorParcela($capital, $i+1);
	   }
	   if($numParcelas > 1){
		   	if($this->tipoJ == 's'){
		   if($this->jurosad > 0 AND $i >= $this->jurosad) {
			   $csjuros =  "Com juros"; }
			   else{ $csjuros = "Sem Juros";}
		$parsela .= '<b>ou '.($i).'x</b> de <b>R$ '.number_format($valorParcela,2,',','.').' '. $csjuros.'</b>';}
		else{
			//Compostos (q deus me ajude)
			  $taxa = $this->taxa / 100;
			    for($i=1; $i < $numParcelas; $i++){
				if($i < $this->jurosad){
					$valorParcs = number_format($capital/$i, 2, ",", ".");
					$qts = $i;
						$parsela = '<b>ou '.$qts.'x</b> de <b>R$ '.$valorParcs.' sem Juros</b>';
				}else{
						$valParcela = $capital * pow((1 + $taxa), ($numParcelas-$qts));
						$valParcela = number_format($valParcela / ($numParcelas-$qts), 2, ",", ".");
						$parsela = '<b>ou '.($numParcelas+$qts-1).'x</b> de <b>R$ '.$valParcela.' com Juros</b>';}
					}
		}
	}
		else{
			$parsela .= '<b>a 1x</b> de <b>R$ '.number_format($valo,2,',','.').'</b>';
		}
		 $parsela .= '</small>';
		return $parsela;
	}

 
   

}